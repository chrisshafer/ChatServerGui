import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import java.awt.event.KeyAdapter;
import javax.swing.JLabel;
import javax.swing.JList;


public class ServerWindow {

	private static JFrame frame;
	private static JTextField textField;
	public static final int LISTENING_PORT = 11516;
	public static final int MAX_CLIENTS = 4;
	public static BufferedReader in = null;
	public static PrintWriter out = null;
	private static DefaultListModel<String> model;
	static JTextArea textPane = null;
	private static JLabel lblConnectedTo;
	private static JList<String> list;
	private JButton btnKick;
	/**
	 * Launch the application.
	 * @throws IOException 
	 * @throws NumberFormatException 
	 */
	public static void main(String[] args) throws NumberFormatException, IOException {
		
		try {
			
			ServerWindow window = new ServerWindow();
			window.frame.setVisible(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Open server socket for listening
				ServerSocket serverSocket = null;
				try {
					serverSocket = new ServerSocket(LISTENING_PORT);
					println("Server started on port " + LISTENING_PORT);
				} catch (IOException se) {
					System.err.println("Can not start listening on port " + LISTENING_PORT);
					se.printStackTrace();
					System.exit(-1);
				} 

				// Start manager
				Manager manager = new Manager();
				
				// read in user accounts
					BufferedReader br = new BufferedReader(new FileReader("users.txt"));
					String currentLine;
					while ((currentLine = br.readLine()) != null) {
						String[] input = currentLine.split(";",5 );
						if(input.length<5)
						{
							println("Input file incorrectly formatted");
							break;
						}
						// creates user and sets values
						User user = new User();
						user.name = input[0];
						user.password = input[1];
						user.region = Integer.parseInt(input[3]);
						user.anycast = input[4].split(",");
						if(input[2].contains(","))
						{
							String[] groups = input[2].split(",");
							for(int i=0; i<groups.length; i++)
								user.group.add(Integer.parseInt(groups[i]));
						}
						else
							user.group.add(Integer.parseInt(input[2]));
						// adds user to the list of possible users
						manager.userDatabase.add(user);
					}
					br.close();


				// Accept and handle client connections
				while (true) {
					try {
						//accept connections
						Socket socket = serverSocket.accept();
						//set sender and listener for the client
						ClientInfo clientInfo = new ClientInfo();
						clientInfo.socket = socket;
						ServerListener serverListener = new ServerListener(clientInfo, manager);
						ServerSender serverSender = new ServerSender(clientInfo, manager);	
						clientInfo.listener = serverListener;
						clientInfo.sender = serverSender;
						serverListener.start();
						serverSender.start();
						println("client connected:"+socket.toString());
						serverSender.sendMessage("type login <username> <password> to login");
						// adds clients to the manager
						manager.addClient(clientInfo);
					} catch (IOException ioe) {
						serverSocket.close();
						ioe.printStackTrace();
					}
				}
				
		
	}

	
	public static void println(String message)
	{
		textPane.setText(textPane.getText()+message+"\n");
		textPane.setCaretPosition(textPane.getDocument().getLength());
	}

	/**
	 * Create the application.
	 */
	public ServerWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 959, 755);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				
			}
		});
		textField.setBounds(21, 666, 581, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Send");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		btnNewButton.setBounds(612, 665, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		textPane = new JTextArea();
		textPane.setEditable(false);
		textPane.setBounds(21, 73, 696, 344);
		textPane.setLineWrap(true);
		textPane.setWrapStyleWord(true);
		textPane.setAutoscrolls(true);
		JScrollPane sp = new JScrollPane(textPane);
		sp.setSize(686, 567);
		sp.setLocation(21, 75);
		frame.getContentPane().add(sp);
		
		lblConnectedTo = new JLabel("Listening Port: "+LISTENING_PORT);
		lblConnectedTo.setBounds(23, 37, 696, 14);
		frame.getContentPane().add(lblConnectedTo);
		
		model = new DefaultListModel<String>();
		list = new JList<String>(model);
		list.setBounds(740, 75, 183, 541);
		frame.getContentPane().add(list);
		
		btnKick = new JButton("Kick");
		btnKick.setBounds(786, 627, 89, 23);
		frame.getContentPane().add(btnKick);
	}
	public static void updateClientList(Vector<ClientInfo> clients)
	{
		model.clear();
		for(int i=0; i<clients.size(); i++)
			model.addElement(clients.get(i).user.name);
		list = new JList<String>(model);
	}
	
}
