import java.io.*;
import java.net.*;

import com.google.gson.Gson;
 
public class ServerListener extends Thread
{
    private Manager manager;
    private ClientInfo info;
    private BufferedReader in;
 
    public ServerListener(ClientInfo info, Manager manager)
    throws IOException
    {
        this.info = info;
        this.manager = manager;
        Socket socket = info.socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }
    public void run()
    {
        try {
           while (!isInterrupted()) {
               String message = in.readLine();
               if (message == null)
                   break;
               processCommand(info,message);
           }
        } catch (IOException ioex) {
           // Problem reading from socket (communication is broken)
        }
        // Communication is broken. Interrupt both listener and sender threads
        ServerWindow.println(info.user.name + " has logged off");
        info.sender.interrupt();
        try {
			manager.deleteClient(info);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   
    //proccesses the first command of the input
    public void processCommand(ClientInfo info, String message) throws IOException
    {
    	Gson gson = new Gson();
    	Command input = gson.fromJson(message, Command.class);
    	if(info.isLoggedIn)
    	switch(input.command)
    	{
    		case 101:
    			manager.dispatchMessageToAll(info, input);
    			break;
    		case 102:
    			manager.dispatchMessageToGroup(info, input);
    			break;
    		case 103:
    			manager.dispatchMessageToUser(info, input);
    			break;
    		case 201:
    			manager.who(info);
    			break;
    		case 202:
    			// TODO create group
    			break;
    		case 302:
    			manager.logout(info);
    			break;
    		default:
    			manager.serverMessageToSender(info, "Input error: command number not recognized");
    			break;
    	}
    	if(!info.isLoggedIn)
    	{
    		if(input.command ==  301)
			manager.login(info, input);
    		else
    		manager.serverMessageToSender(info,"You need to login ");
    	}
    		
    }
 
}