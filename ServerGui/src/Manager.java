
import java.io.IOException;
import java.util.*;
 
public class Manager
{
    public Vector<ClientInfo> clients = new Vector<ClientInfo>();
    public Vector<User> userDatabase = new Vector<User>();
    
    public void addClient(ClientInfo info) throws IOException
    {
    	if(this.clients.size() < ServerWindow.MAX_CLIENTS)
        clients.add(info);
    	else
    	{
    		info.sender.sendMessageToClientNow("Maximum number of connections reached");
    		info.socket.close();
    	}
    	
    }
    
    public void deleteClient(ClientInfo info) throws IOException
    {	
	        int clientIndex = clients.indexOf(info);
	        if (clientIndex >= 0 && clientIndex < clients.size()){
		        clients.get(clientIndex).socket.close();
		        clients.removeElement(info);
	        }
    }
 
    public void serverMessageToAll(String message)
    {
        for (int i=0; i<clients.size(); i++) {
            ClientInfo clientInfo = (ClientInfo) clients.get(i);
            if(clientInfo.isLoggedIn)
            clientInfo.sender.sendMessage("SERVER : " + message);
         }
    }
    
    public void serverMessageToSender(ClientInfo info, String message)
    {
        info.sender.sendMessage("SERVER : " + message);
    }
    
    public void who(ClientInfo info)
    {
        String message = "WHO REQUEST :\n";
        for(int i=0; i<clients.size(); i++)
        {	if(clients.get(i).isLoggedIn)
        	message += clients.get(i).user.name+"\n";
        }
        serverMessageToSender(info,message);
    }
    
    public void logout(ClientInfo info) throws IOException
    {
        String name = info.user.name;
        deleteClient(info);
        serverMessageToAll(name+" has logged off");
        ServerWindow.println(name+" has logged off");
    }
    public void login(ClientInfo info, Command message)
    {

    		// check the the user list for the username and password
    		for(int i=0; i<this.userDatabase.size(); i++)
    		{
    			if(this.userDatabase.get(i).name.equals(message.text) && this.userDatabase.get(i).password.equals(message.commandData))
    			{	
    				info.user = this.userDatabase.get(i);
    				info.isLoggedIn = true;
    				ServerWindow.println(info.user.name+" logged in");
    				this.serverMessageToSender(info, "login successsfull");
    				this.serverMessageToAll(info.user.name+ " has logged in");
        			ServerWindow.updateClientList(this.clients);
    			}
    			
    		}
    		if(info.isLoggedIn != true)
    			this.serverMessageToSender(info, "login unsuccessful");
    		
    
    }
    //broadcast
    public synchronized void dispatchMessageToAll(ClientInfo info, Command message)
    {
        for (int i=0; i<clients.size(); i++) {
            ClientInfo clientInfo = (ClientInfo) clients.get(i);
            if(clientInfo.isLoggedIn)
            clientInfo.sender.sendMessage("[All]:"+info.user.name + " : " + message.text);
            ServerWindow.println("[All]:"+info.user.name + " : " + message.text);
         }
    }
    //unicast
	public void dispatchMessageToUser(ClientInfo info, Command message) {
		String name = message.commandData;
		for (int i=0; i<clients.size(); i++) {
	           ClientInfo clientInfo = (ClientInfo) clients.get(i);
	           if(clientInfo.user.name.equals(name))
	           {
	           clientInfo.sender.sendMessage("[Private]:"+info.user.name+": "+message.text);
	           ServerWindow.println("[To-"+clientInfo.user.name+"]:"+info.user.name+": "+message.text);
	           return;
	           }
	        }
		serverMessageToSender(info,"User not online");
		
	}
	//multicast
	public void dispatchMessageToGroup(ClientInfo info, Command message) {
		String group = message.commandData;
		if(info.user.group.contains(group)){
		for (int i=0; i<clients.size(); i++) {
	           ClientInfo clientInfo = (ClientInfo) clients.get(i);
	           if(clientInfo.user.group.contains(group))
	           clientInfo.sender.sendMessage("[Group-"+group+"]:"+info.user.name+": "+message.text);
	           ServerWindow.println("[Group-"+group+"]:"+info.user.name+": "+message.text);
	     }
		}
		else
			serverMessageToSender(info,"You are not a member of group : "+group);
			
			
		
	}
	
	
}